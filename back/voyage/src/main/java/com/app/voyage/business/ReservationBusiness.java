package com.app.voyage.business;

import com.app.voyage.dto.Panier;
import com.app.voyage.dto.Reservation;
import com.app.voyage.dto.model.CreationReservationModel;
import com.app.voyage.repository.ReservationRepository;
import jakarta.inject.Inject;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class ReservationBusiness {

    @Inject
    UtilisateurBusiness utilisateurBusiness;
    @Inject
    VoyageBusiness voyageBusiness;
    @Inject
    PanierBusiness panierBusiness;
    @Inject
    ReservationRepository reservationRepository;

    public Reservation createReservation(CreationReservationModel creationReservationModel) {
        String mail = creationReservationModel.getIdUtilisateur();
        // trouver utilisateur
        this.utilisateurBusiness.getUtilisateur(mail);
        // trouver voyage
        this.voyageBusiness.getVoyage(creationReservationModel.getIdVoyage());

        String idPanier = "";
        // si panier ajouter // si non créer panier
        Panier p = this.panierBusiness.getPanierEnCours(mail);
        if(p == null) {
            idPanier = this.panierBusiness.createPanierEnCours(mail);
        } else {
            idPanier = p.getId();
        }

        return this.reservationRepository.createReservation(creationReservationModel, idPanier);
    }

    public List<Reservation> getReservationPanier(String idPanier) {
        return this.reservationRepository.getReservationPanier(idPanier);
    }

    public void supprimerReservation(String id) {
        this.reservationRepository.supprimerReservation(id);
    }
}
