package com.app.voyage.mapper;

import com.app.voyage.dto.Image;
import com.app.voyage.dto.Ville;
import com.app.voyage.dto.Voyage;
import com.app.voyage.entity.VoyageEntity;

public class VoyageMapper {
    public static Voyage toDto(VoyageEntity voyageEntity) {
        Voyage dto = new Voyage();

        dto.setId(voyageEntity.getId());
        dto.setNom(voyageEntity.getNom());
        dto.setDescription(voyageEntity.getDescription());
        dto.setPrix(voyageEntity.getPrix());

        Ville v = new Ville();
        v.setId(voyageEntity.getVilleId());
        dto.setVille(v);

        Image i = new Image();
        i.setId(voyageEntity.getImageId());
        dto.setImage(i);

        return dto;
    }

    public static VoyageEntity toEntity(Voyage voyage) {
        VoyageEntity entity = new VoyageEntity();

        entity.setId(voyage.getId());
        entity.setNom(voyage.getNom());
        entity.setDescription(voyage.getDescription());
        entity.setPrix(voyage.getPrix());

        entity.setVilleId(voyage.getVille().getId());
        entity.setImageId(voyage.getImage().getId());

        return entity;
    }
}
