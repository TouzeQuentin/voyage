package com.app.voyage.business;

import com.app.voyage.dto.Pays;
import com.app.voyage.entity.PaysEntity;
import com.app.voyage.repository.PaysRepository;
import jakarta.inject.Inject;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class PaysBusiness {
    @Inject
    PaysRepository paysRepository;

    public List<Pays> getPays() {
        return paysRepository.getAllPays();
    }

    public void createPays(PaysEntity paysEntity) {

        Pays p = new Pays();
        p.setId(UUID.randomUUID().toString());
        p.setNom(paysEntity.getNom());
        p.setDescription(paysEntity.getDescription());

        paysRepository.createPays(p);
    }
}
