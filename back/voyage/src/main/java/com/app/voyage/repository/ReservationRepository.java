package com.app.voyage.repository;

import com.app.voyage.dto.Image;
import com.app.voyage.dto.Reservation;
import com.app.voyage.dto.Voyage;
import com.app.voyage.dto.model.CreationReservationModel;
import com.app.voyage.entity.ReservationEntity;
import jakarta.inject.Inject;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ReservationRepository {

    private final static String SQL_GET_RESERVATION = "SELECT * FROM RESERVATION WHERE ID = :id;";
    private final static String SQL_GET_RESERVATION_BY_PANIER_ID = "SELECT * FROM RESERVATION WHERE PANIER_ID = :id;";
    private final static String SQL_CREATE_RESERVATION = "INSERT INTO RESERVATION (ID, DATE_DEBUT, DATE_FIN, PANIER_ID, VOYAGE_ID) VALUES (:id, :dateDebut, :dateFin, :panier, :voyage)";
    private final static String SQL_DELETE_RESERVATION = "DELETE FROM RESERVATION WHERE ID = :id;";

    @Inject
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Inject
    VoyageRepository voyageRepository;

    public Reservation createReservation(CreationReservationModel creationModel, String idPanier) {
        var params = new HashMap<String, Object>();

        String id = UUID.randomUUID().toString();
        params.put("id", id);
        params.put("dateDebut", creationModel.getDateDebut());
        params.put("dateFin", creationModel.getDateFin());
        params.put("panier", idPanier);
        params.put("voyage", creationModel.getIdVoyage());

        this.jdbcTemplate.update(SQL_CREATE_RESERVATION, params);

        params = new HashMap<String, Object>();

        params.put("id", id);
        return (Reservation) this.jdbcTemplate.query(SQL_GET_RESERVATION, params, new BeanPropertyRowMapper(Reservation.class)).get(0);
    }

    public List<Reservation> getReservationPanier(String idPanier) {
        var params = new HashMap<String, Object>();

        params.put("id", idPanier);
        List<ReservationEntity> listReservationEntity = this.jdbcTemplate.query(SQL_GET_RESERVATION_BY_PANIER_ID, params, new BeanPropertyRowMapper(ReservationEntity.class));

        List<Reservation> reservationList = new ArrayList<>();
        for (ReservationEntity r: listReservationEntity) {
            Reservation reservation = new Reservation();
            reservation.setId(r.getId());
            reservation.setDateDebut(new Date(r.getDateDebut().getTime()));
            reservation.setDateFin(new Date(r.getDateFin().getTime()));
            reservation.setVoyage(voyageRepository.getVoyage(r.getVoyageId()));

            reservationList.add(reservation);
        }
        return reservationList;
    }

    public void supprimerReservation(String id) {
        var params = new HashMap<String, Object>();

        params.put("id", id);
        this.jdbcTemplate.update(SQL_DELETE_RESERVATION, params);
    }
}
