package com.app.voyage.dto.model;

import lombok.Data;

import java.util.Date;

@Data
public class CreationReservationModel {
    private String idUtilisateur;
    private String idVoyage;
    private Date dateDebut;
    private Date dateFin;
}
