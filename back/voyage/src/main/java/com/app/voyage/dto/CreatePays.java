package com.app.voyage.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CreatePays {

    String nom;

    String description;
}
