package com.app.voyage.repository;

import com.app.voyage.dto.Pays;
import com.app.voyage.dto.Utilisateur;
import jakarta.inject.Inject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
public class PaysRepository {
    private final static String SQL_CREATE_PAYS = "INSERT INTO PAYS (ID, NOM, DESCRIPTION) VALUES (:id, :nom, :description)";
    private final static String SQL_GET_ALL_PAYS = "SELECT * FROM PAYS";
    private final static String SQL_GET_PAYS = "SELECT * FROM PAYS WHERE ID = :id;";

    @Inject
    private NamedParameterJdbcTemplate jdbcTemplate;

    public void createPays(Pays pays) {
        var params = new HashMap<String, Object>();

        params.put("id", pays.getId());
        params.put("nom", pays.getNom());
        params.put("description", pays.getDescription());

        this.jdbcTemplate.update(SQL_CREATE_PAYS, params);
    }

    public List<Pays> getAllPays() {
        return this.jdbcTemplate.query(SQL_GET_ALL_PAYS, new BeanPropertyRowMapper(Pays.class));
    }

    public Pays getPays(String id) {
        var params = new HashMap<String, Object>();

        params.put("id", id);
        return (Pays) this.jdbcTemplate.query(SQL_GET_PAYS, params, new BeanPropertyRowMapper(Pays.class)).get(0);
    }
}
