package com.app.voyage.controller;

import com.app.voyage.business.UtilisateurBusiness;
import com.app.voyage.dto.Utilisateur;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Controller
@Path("/utilisateur")
@CrossOrigin(origins = "http://localhost:4200")
public class UtilisateurController {
    @Inject
    UtilisateurBusiness utilisateurBusiness;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getUtilisateurs() {
        List<Utilisateur> listeUtilisateurs = this.utilisateurBusiness.getUtilisateurs();
        return Response.ok(listeUtilisateurs).build();
    }

    @POST
    @Path("/connexion")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getConnexionOk(Utilisateur u) {
        return Response.ok(this.utilisateurBusiness.getUtilisateurConnexion(u.getMail(), u.getMdp())).build();
    }
}
