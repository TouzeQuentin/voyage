package com.app.voyage.repository;

import com.app.voyage.dto.Pays;
import com.app.voyage.dto.Ville;
import com.app.voyage.dto.Voyage;
import com.app.voyage.entity.VilleEntity;
import com.app.voyage.entity.VoyageEntity;
import com.app.voyage.mapper.VilleMapper;
import com.app.voyage.mapper.VoyageMapper;
import jakarta.inject.Inject;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Component
public class VoyageRepository {
    private final static String SQL_CREATE_VOYAGE = "INSERT INTO VOYAGE (ID, NOM, DESCRIPTION, PRIX, VILLE_ID, IMAGE_ID) VALUES (:id, :nom, :description, :prix, :villeId, :imageId)";
    private final static String SQL_GET_ALL_VOYAGE = "SELECT * FROM VOYAGE";
    private final static String SQL_GET_VOYAGE_BY_ID = "SELECT * FROM VOYAGE WHERE ID = :id;";
    @Inject
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Inject
    private VilleRepository villeRepository;
    @Inject
    private ImageRepository imageRepository;

    public void createVoyage(Voyage v) {
        var params = new HashMap<String, Object>();

        String id = v.getId()!=null || v.getId().isEmpty()?v.getId(): UUID.randomUUID().toString();
        params.put("id", id);
        params.put("nom", v.getNom());
        params.put("description", v.getDescription());
        params.put("prix", v.getPrix());
        params.put("villeId", v.getVille().getId());
        if(v.getImage() != null) {
            params.put("imageId", v.getImage().getId());
        } else {
            params.put("imageId", null);
        }

        this.jdbcTemplate.update(SQL_CREATE_VOYAGE, params);
    }

    public List<Voyage> getVoyages() {
        List<VoyageEntity> list = this.jdbcTemplate.query(SQL_GET_ALL_VOYAGE, new BeanPropertyRowMapper(VoyageEntity.class));

        List<Voyage> listVoyage = new ArrayList<>();
        for(VoyageEntity v: list) {
            Voyage voyage = VoyageMapper.toDto(v);

            voyage.setVille(villeRepository.getVille(v.getVilleId()));
            voyage.setImage(imageRepository.getImage(v.getImageId()));
            listVoyage.add(voyage);
        }

        return listVoyage;
    }

    public Voyage getVoyage(String id) {
        var params = new HashMap<String, Object>();

        params.put("id", id);

        var a = this.jdbcTemplate.query(SQL_GET_VOYAGE_BY_ID, params, new BeanPropertyRowMapper(VoyageEntity.class));

        VoyageEntity voyageEntity = (VoyageEntity) this.jdbcTemplate.query(SQL_GET_VOYAGE_BY_ID, params, new BeanPropertyRowMapper(VoyageEntity.class)).get(0);

        Voyage voyage = VoyageMapper.toDto(voyageEntity);
        voyage.setVille(villeRepository.getVille(voyageEntity.getVilleId()));
        voyage.setImage(imageRepository.getImage(voyageEntity.getImageId()));

        return voyage;
    }
}
