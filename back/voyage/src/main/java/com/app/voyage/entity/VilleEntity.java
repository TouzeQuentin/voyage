package com.app.voyage.entity;

import lombok.Data;

@Data
public class VilleEntity {
    private String id;
    private String nom;
    private String description;
    private String paysId;
}
