package com.app.voyage.repository;

import com.app.voyage.dto.Message;
import com.app.voyage.dto.Panier;
import jakarta.inject.Inject;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Component
public class MessageRepository {
    private final static String SQL_GET_MESSAGE_VOYAGE = "SELECT * FROM MESSAGE WHERE VOYAGE_ID = :id;";
    private final static String SQL_ADD_MESSAGE = "INSERT INTO MESSAGE (ID, NOTE, TEXT, VOYAGE_ID, UTILISATEUR_MAIL) VALUES (:id, :note, :text, :voyage, :mail)";

    @Inject
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<Message> getMessages(String idVoyage) {
        var params = new HashMap<String, Object>();

        params.put("id", idVoyage);
        return this.jdbcTemplate.query(SQL_GET_MESSAGE_VOYAGE,params, new BeanPropertyRowMapper(Message.class));
    }

    public void createMessage(Message m) {
        var params = new HashMap<String, Object>();

        params.put("id", UUID.randomUUID().toString());
        params.put("note", m.getNote());
        params.put("text", m.getText());
        params.put("voyage", m.getVoyageId());
        params.put("mail", m.getUtilisateurMail());

        this.jdbcTemplate.update(SQL_ADD_MESSAGE, params);
    }
}
