package com.app.voyage.business;

import com.app.voyage.business.Shared.ImageService;
import com.app.voyage.dto.Image;
import com.app.voyage.repository.ImageRepository;
import jakarta.inject.Inject;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Component
public class ImageBusiness {

    @Inject
    ImageRepository imageRepository;
    @Inject
    ImageService imageService;

    public Image createImage(MultipartFile file) throws IOException {

        Image i = new Image();
        i.setImage(this.imageService.resizeAndFormatImage(file));

        return this.imageRepository.createImage(i);
    }
}
