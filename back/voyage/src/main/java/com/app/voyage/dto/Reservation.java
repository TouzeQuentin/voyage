package com.app.voyage.dto;

import lombok.Data;

import java.util.Date;

@Data
public class Reservation {
    private String id;
    private Date dateDebut;
    private Date dateFin;
    private Panier panier;
    private Voyage voyage;
}
