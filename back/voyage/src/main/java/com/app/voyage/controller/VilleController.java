package com.app.voyage.controller;

import com.app.voyage.business.VilleBusiness;
import com.app.voyage.dto.Ville;
import com.app.voyage.mapper.VilleMapper;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Controller
@Path("/villes")
@CrossOrigin(origins = "http://localhost:4200")
public class VilleController {
    @Inject
    VilleBusiness villeBusiness;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVilles() {
        List<Ville> listeVilles = this.villeBusiness.getVilles();
        return Response.ok(listeVilles).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createVille(Ville v) {
        this.villeBusiness.createVille(VilleMapper.toEntity(v));
        return Response.ok().build();
    }
}
