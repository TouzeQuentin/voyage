package com.app.voyage.configuration;

import jakarta.ws.rs.ApplicationPath;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;

@Configuration
@ApplicationPath("/voyage/v1")
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        packages("com.app.voyage.controller");
        packages("com.app.voyage.filter");
        packages("com.app.voyage.exception");
    }
}
