package com.app.voyage.dto.model;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CreationImageModel {
    private String id;

    private String nom;

    private MultipartFile image;
}
