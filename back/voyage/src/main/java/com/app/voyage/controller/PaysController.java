package com.app.voyage.controller;

import com.app.voyage.business.PaysBusiness;
import com.app.voyage.business.Shared.ImageService;
import com.app.voyage.dto.CreatePays;
import com.app.voyage.dto.Image;
import com.app.voyage.dto.Pays;
import com.app.voyage.dto.Utilisateur;
import com.app.voyage.mapper.PaysMapper;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
@Path("/pays")
@CrossOrigin(origins = "http://localhost:4200")
public class PaysController {

    @Inject
    PaysBusiness paysBusiness;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPays() {
        List<Pays> listePays = this.paysBusiness.getPays();
        return Response.ok(listePays).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createPays(Pays p) throws IOException {
        this.paysBusiness.createPays(PaysMapper.toEntity(p));
        return Response.ok().build();
    }
}
