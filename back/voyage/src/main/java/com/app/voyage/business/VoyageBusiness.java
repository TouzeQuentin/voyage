package com.app.voyage.business;

import com.app.voyage.dto.Voyage;
import com.app.voyage.repository.VoyageRepository;
import jakarta.inject.Inject;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VoyageBusiness {

    @Inject
    VoyageRepository voyageRepository;

    public List<Voyage> getVoyages() {
        return voyageRepository.getVoyages();
    }

    public Voyage getVoyage(String id) {
        return voyageRepository.getVoyage(id);
    }

    public void createVoyage(Voyage voyage) {
        this.voyageRepository.createVoyage(voyage);
    }
}
