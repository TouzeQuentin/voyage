package com.app.voyage.business;

import com.app.voyage.dto.Ville;
import com.app.voyage.entity.VilleEntity;
import com.app.voyage.repository.VilleRepository;
import jakarta.inject.Inject;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.UUID;

@Component
public class VilleBusiness {

    @Inject
    VilleRepository villeRepository;

    public List<Ville> getVilles() {
        return villeRepository.getVilles();
    }

    public void createVille(VilleEntity villeEntity) {

        Ville v = new Ville();
        v.setId(UUID.randomUUID().toString());
        v.setNom(villeEntity.getNom());
        v.setDescription(villeEntity.getDescription());

        villeRepository.createVille(v);
    }
}
