package com.app.voyage.business;

import com.app.voyage.dto.Message;
import com.app.voyage.repository.MessageRepository;
import jakarta.inject.Inject;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class MessageBusiness {
    @Inject
    MessageRepository messageRepository;

    public List<Message> getMessages(String idVoyage) {
        return this.messageRepository.getMessages(idVoyage);
    }

    public void createMessage(Message m) {
        this.messageRepository.createMessage(m);
    }
}
