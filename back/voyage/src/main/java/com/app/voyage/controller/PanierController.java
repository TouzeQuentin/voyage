package com.app.voyage.controller;

import com.app.voyage.business.PanierBusiness;
import com.app.voyage.dto.Panier;
import com.app.voyage.dto.Pays;
import com.app.voyage.dto.model.PanierAvecReservationModel;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Controller
@Path("/panier")
@CrossOrigin(origins = "http://localhost:4200")
public class PanierController {
    @Inject
    PanierBusiness panierBusiness;
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPaniers(@QueryParam("mail") String mailUtilisateur) {
        List<PanierAvecReservationModel> listePaniers = this.panierBusiness.getPaniers(mailUtilisateur);
        return Response.ok(listePaniers).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response validerPanier(@QueryParam("id") String idPanier) {
        this.panierBusiness.validerPanier(idPanier);
        return Response.ok().build();
    }
}
