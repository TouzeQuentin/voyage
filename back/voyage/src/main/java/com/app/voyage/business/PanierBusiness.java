package com.app.voyage.business;

import com.app.voyage.dto.Panier;
import com.app.voyage.dto.model.PanierAvecReservationModel;
import com.app.voyage.repository.PanierRepository;
import com.app.voyage.repository.ReservationRepository;
import jakarta.inject.Inject;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PanierBusiness {
    @Inject
    PanierRepository panierRepository;
    @Inject
    ReservationRepository reservationRepository;

    public List<PanierAvecReservationModel> getPaniers(String mailUtilisateur) {
        List<Panier> listePanier = this.panierRepository.getPaniers(mailUtilisateur);
        List<PanierAvecReservationModel> liste = new ArrayList<>();

        for(Panier p: listePanier) {
            PanierAvecReservationModel panierAvecReservationModel = new PanierAvecReservationModel();

            panierAvecReservationModel.setId(p.getId());
            panierAvecReservationModel.setEtat(p.getEtat());

            panierAvecReservationModel.setReservations(this.reservationRepository.getReservationPanier(p.getId()));

            liste.add(panierAvecReservationModel);
        }
        return liste;
    }
    Panier getPanierEnCours(String mailUtilisateur) {
        return this.panierRepository.getPanierEnCours(mailUtilisateur);
    }

    String createPanierEnCours(String mailUtilisateur) {
        return this.panierRepository.createPanierEnCours(mailUtilisateur);
    }

    public void validerPanier(String id) {
        this.panierRepository.validerPanier(id);
    }

}
