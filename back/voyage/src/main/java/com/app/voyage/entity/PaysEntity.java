package com.app.voyage.entity;

import lombok.Data;

@Data
public class PaysEntity {
    private String id;
    private String nom;
    private String description;
}
