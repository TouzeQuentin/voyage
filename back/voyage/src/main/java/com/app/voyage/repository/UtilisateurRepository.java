package com.app.voyage.repository;

import com.app.voyage.dto.Utilisateur;
import jakarta.inject.Inject;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

@Component
public class UtilisateurRepository {
    private final static String SQL_GET_ALL_UTILISATEUR = "SELECT * FROM UTILISATEUR";
    private final static String SQL_GET_UTILISATEUR_CONNEXION = "SELECT * FROM UTILISATEUR WHERE MAIL=:mail AND MAIL =:mdp;";
    private final static String SQL_GET_UTILISATEUR = "SELECT * FROM UTILISATEUR WHERE MAIL=:mail;";

    @Inject
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<Utilisateur> getUtilisateurs() {
        return this.jdbcTemplate.query(SQL_GET_ALL_UTILISATEUR, new BeanPropertyRowMapper<>(Utilisateur.class));
    }

    public Utilisateur getUtilisateur(String mail) {
        var params = new HashMap<String, Object>();

        params.put("mail", mail);

        List<Utilisateur> list = this.jdbcTemplate.query(SQL_GET_UTILISATEUR, params, new BeanPropertyRowMapper<>(Utilisateur.class));
        if(list.isEmpty()) {
            return new Utilisateur();
        }
        return list.get(0);
    }

    public Utilisateur getUtilisateurConnexion(String mail, String mdp) {
        var params = new HashMap<String, Object>();

        params.put("mail", mail);
        params.put("mdp", mdp);
        List<Utilisateur> list = this.jdbcTemplate.query(SQL_GET_UTILISATEUR_CONNEXION, params, new BeanPropertyRowMapper<>(Utilisateur.class));
        if(list.isEmpty()) {
            return new Utilisateur();
        }
        return list.get(0);
    }

}
