package com.app.voyage.dto;

import lombok.Data;

@Data
public class Message {
    private String id;
    private double note;
    private String text;
    private String voyageId;
    private String utilisateurMail;
}
