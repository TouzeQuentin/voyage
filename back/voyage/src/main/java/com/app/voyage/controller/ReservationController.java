package com.app.voyage.controller;

import com.app.voyage.business.ReservationBusiness;
import com.app.voyage.dto.model.CreationReservationModel;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@Controller
@Path("/reservation")
@CrossOrigin(origins = "http://localhost:4200")
public class ReservationController {
    @Inject
    ReservationBusiness reservationBusiness;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createReservation(CreationReservationModel creationReservationModel) {
        return Response.ok(this.reservationBusiness.createReservation(creationReservationModel)).build();
    }

    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response supprimerReservation(@QueryParam("id") String id) {
        System.out.println(id);
        this.reservationBusiness.supprimerReservation(id);
        return Response.ok().build();
    }
}
