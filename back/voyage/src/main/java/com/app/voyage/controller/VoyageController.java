package com.app.voyage.controller;

import com.app.voyage.business.VoyageBusiness;
import com.app.voyage.dto.Ville;
import com.app.voyage.dto.Voyage;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
@Path("/voyages")
@CrossOrigin(origins = "http://localhost:4200")
public class VoyageController {

    @Inject
    VoyageBusiness voyageBusiness;

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVoyages(@QueryParam("id") String id) {
        if( id!= null) {
            Voyage voyage = this.voyageBusiness.getVoyage(id);
            return Response.ok(voyage).build();
        }
        List<Voyage> listeVoyages = this.voyageBusiness.getVoyages();
        return Response.ok(listeVoyages).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createVoyage(Voyage voyage) {
        this.voyageBusiness.createVoyage(voyage);
        return Response.ok().build();
    }

}
