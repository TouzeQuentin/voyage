package com.app.voyage.controller;

import com.app.voyage.business.MessageBusiness;
import com.app.voyage.dto.Message;
import com.app.voyage.dto.model.PanierAvecReservationModel;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@Controller
@Path("/messages")
@CrossOrigin(origins = "http://localhost:4200")
public class MessageController {

    @Inject
    MessageBusiness messageBusiness;
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMessage(@QueryParam("id") String idVoyage) {
        List<Message> liste = this.messageBusiness.getMessages(idVoyage);
        return Response.ok(liste).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createMessage(Message message) {
        this.messageBusiness.createMessage(message);
        return Response.ok().build();
    }
}
