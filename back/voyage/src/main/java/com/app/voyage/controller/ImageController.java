package com.app.voyage.controller;

import com.app.voyage.business.ImageBusiness;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Controller
@Path("/images")
@CrossOrigin(origins = "http://localhost:4200")
public class ImageController {

    @Inject
    ImageBusiness imageBusiness;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createImage(@RequestParam("file") MultipartFile file) throws IOException {
        return Response.ok(this.imageBusiness.createImage(file)).build();
    }
}
