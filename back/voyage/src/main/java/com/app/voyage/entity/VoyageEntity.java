package com.app.voyage.entity;

import lombok.Data;

@Data
public class VoyageEntity {
    private String id;
    private String nom;
    private String description;
    private double prix;
    private String villeId;
    private String imageId;
}
