package com.app.voyage.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalTime;

@Data
public class Utilisateur implements Serializable {

    @JsonProperty("mail")
    private String mail;

    @JsonProperty("mdp")
    private String mdp;

    @JsonProperty("numero")
    private String numero;

    @JsonProperty("date_creation")
    private LocalTime dateCreation;

    @JsonProperty("statut")
    private char statut;
}
