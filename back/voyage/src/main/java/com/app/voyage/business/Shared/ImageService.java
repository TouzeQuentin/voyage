package com.app.voyage.business.Shared;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

@Service
public class ImageService {

    public byte[] resizeAndFormatImage(MultipartFile file) throws IOException {
        byte[] fileBytes = file.getBytes();

        ByteArrayInputStream bis = new ByteArrayInputStream(fileBytes);
        BufferedImage bufferedImage = ImageIO.read(bis);

        int newWidth = 300; // Définissez la largeur souhaitée
        int newHeight = 250; // Définissez la hauteur souhaitée

        Image scaledImage = bufferedImage.getScaledInstance(newWidth, newHeight, Image.SCALE_SMOOTH);
        BufferedImage newBufferedImage = new BufferedImage(newWidth, newHeight, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = newBufferedImage.createGraphics();
        g.drawImage(scaledImage, 0, 0, null);
        g.dispose();

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImageIO.write(newBufferedImage, "png", bos);
        return bos.toByteArray();
    }
}
