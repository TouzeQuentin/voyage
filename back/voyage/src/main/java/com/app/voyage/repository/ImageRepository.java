package com.app.voyage.repository;

import com.app.voyage.dto.Image;
import com.app.voyage.dto.Ville;
import jakarta.inject.Inject;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Component
public class ImageRepository {

    private final static String SQL_GET_IMAGE = "SELECT * FROM IMAGE WHERE ID = :id";
    private final static String SQL_ADD_IMAGE = "NSERT INTO IMAGE (ID, NOM, IMAGE) VALUES (:id, :nom, :image)";

    @Inject
    private NamedParameterJdbcTemplate jdbcTemplate;

    public Image getImage(String id) {
        var params = new HashMap<String, Object>();

        params.put("id", id);

        return (Image) this.jdbcTemplate.query(SQL_GET_IMAGE, params, new BeanPropertyRowMapper(Image.class)).get(0);
    }

    public Image createImage(Image image) {
        var params = new HashMap<String, Object>();

        String id = UUID.randomUUID().toString();
        params.put("id", id);
        params.put("nom", id);
        params.put("image", image.getImage());

        this.jdbcTemplate.update(SQL_ADD_IMAGE, params);

        params = new HashMap<String, Object>();
        params.put("id", id);
        return (Image) this.jdbcTemplate.query(SQL_GET_IMAGE, params, new BeanPropertyRowMapper(Image.class)).get(0);
    }
}
