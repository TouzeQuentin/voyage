package com.app.voyage.mapper;

import com.app.voyage.dto.Pays;
import com.app.voyage.entity.PaysEntity;

public class PaysMapper {
    public static Pays toDto(PaysEntity paysEntity) {
        Pays dto = new Pays();

        dto.setId(paysEntity.getId());
        dto.setNom(paysEntity.getNom());
        dto.setDescription(paysEntity.getDescription());

        return dto;
    }

    public static PaysEntity toEntity(Pays pays) {
        PaysEntity entity = new PaysEntity();

        entity.setId(pays.getId());
        entity.setNom(pays.getNom());
        entity.setDescription(pays.getDescription());

        return entity;
    }
}
