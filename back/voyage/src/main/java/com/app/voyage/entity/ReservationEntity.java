package com.app.voyage.entity;

import lombok.Data;

import java.sql.Timestamp;

@Data
public class ReservationEntity {
    private String id;
    private Timestamp dateDebut;
    private Timestamp dateFin;
    private String panierId;
    private String voyageId;
}
