package com.app.voyage.business;

import com.app.voyage.dto.Utilisateur;
import com.app.voyage.repository.UtilisateurRepository;
import jakarta.inject.Inject;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UtilisateurBusiness {
    @Inject
    UtilisateurRepository utilisateurRepository;

    public List<Utilisateur> getUtilisateurs() {
        return utilisateurRepository.getUtilisateurs();
    }

    public Utilisateur getUtilisateur(String mail) {
        return utilisateurRepository.getUtilisateur(mail);
    }

    public Utilisateur getUtilisateurConnexion(String mail, String mdp) {
        return this.utilisateurRepository.getUtilisateurConnexion(mail, mdp);
    }
}
