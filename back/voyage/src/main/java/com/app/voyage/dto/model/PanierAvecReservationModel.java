package com.app.voyage.dto.model;

import com.app.voyage.dto.Reservation;
import lombok.Data;

import java.util.List;

@Data
public class PanierAvecReservationModel {
    private String id;
    private char etat;
    private List<Reservation> reservations;
}
