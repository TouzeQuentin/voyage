package com.app.voyage.controller;

import com.app.voyage.dto.Reponse;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.core.Response;
import org.springframework.stereotype.Controller;

@Controller
@Path("/health")
public class HealthController {
    @GET
    public Response getHealth() {
        Reponse reponse = new Reponse();
        reponse.setReponse("Yeah I am fine :)");
        return Response.ok(reponse).build();
    }
}
