package com.app.voyage.dto;

import lombok.Data;

@Data
public class Panier {
    private String id;
    private char etat;
    private Utilisateur utilisateur;
}
