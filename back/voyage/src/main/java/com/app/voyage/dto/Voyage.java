package com.app.voyage.dto;

import lombok.Data;

@Data
public class Voyage {
    private String id;
    private String nom;
    private String description;
    private double prix;
    private Ville ville;
    private Image image;
}
