package com.app.voyage.repository;

import com.app.voyage.dto.Image;
import com.app.voyage.dto.Panier;
import com.app.voyage.dto.Pays;
import jakarta.inject.Inject;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@Component
public class PanierRepository {
    private final static String SQL_GET_ALL_PANIER = "SELECT * FROM PANIER;";
    private final static String SQL_GET_PANIER_UTILISATEUR = "SELECT * FROM PANIER WHERE UTILISATEUR_MAIL = :mail;";
    private final static String SQL_GET_PANIER_EN_COURS = "SELECT * FROM PANIER WHERE UTILISATEUR_MAIL = :mail AND ETAT = 'C';";
    private final static String SQL_CREATE_PANIER = "INSERT INTO PANIER (ID, ETAT, UTILISATEUR_MAIL) VALUES (:id, 'C', :mail)";
    private final static String SQL_VALIDER_PANIER = "UPDATE PANIER SET ETAT = 'H' WHERE ID = :id";

    @Inject
    private NamedParameterJdbcTemplate jdbcTemplate;

    public List<Panier> getPaniers(String mailUtilisateur) {
        var params = new HashMap<String, Object>();

        params.put("mail", mailUtilisateur);
        return this.jdbcTemplate.query(SQL_GET_PANIER_UTILISATEUR,params, new BeanPropertyRowMapper(Panier.class));
    }
    public Panier getPanierEnCours(String mailUtilisateur) {
        var params = new HashMap<String, Object>();

        params.put("mail", mailUtilisateur);

        List<Panier> list = this.jdbcTemplate.query(SQL_GET_PANIER_EN_COURS, params, new BeanPropertyRowMapper(Panier.class));
        return list.isEmpty()? null: list.get(0);
    }

    public String createPanierEnCours(String mailUtilisateur) {
        var params = new HashMap<String, Object>();

        String id = UUID.randomUUID().toString();
        params.put("id", id);
        params.put("mail", mailUtilisateur);

        this.jdbcTemplate.update(SQL_CREATE_PANIER, params);
        return id;
    }

    public void validerPanier(String idPanier) {
        var params = new HashMap<String, Object>();

        params.put("id", idPanier);

        this.jdbcTemplate.update(SQL_VALIDER_PANIER, params);
    }
}
