package com.app.voyage.mapper;

import com.app.voyage.dto.Pays;
import com.app.voyage.dto.Ville;
import com.app.voyage.entity.VilleEntity;

public class VilleMapper {
    public static Ville toDto(VilleEntity villeEntity) {
        Ville dto = new Ville();

        dto.setId(villeEntity.getId());
        dto.setNom(villeEntity.getNom());
        dto.setDescription(villeEntity.getDescription());
        Pays p = new Pays();
        p.setId(villeEntity.getPaysId());
        dto.setPays(p);

        return dto;
    }

    public static VilleEntity toEntity(Ville ville) {
        VilleEntity entity = new VilleEntity();

        entity.setId(ville.getId());
        entity.setNom(ville.getNom());
        entity.setDescription(ville.getDescription());
        entity.setPaysId(ville.getPays().getId());

        return entity;
    }
}