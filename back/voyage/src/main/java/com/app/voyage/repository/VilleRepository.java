package com.app.voyage.repository;

import com.app.voyage.dto.Ville;
import com.app.voyage.entity.VilleEntity;
import com.app.voyage.mapper.VilleMapper;
import jakarta.inject.Inject;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Component
public class VilleRepository {
    private final static String SQL_CREATE_VILLE = "INSERT INTO VILLE (ID, NOM, DESCRIPTION, PAYS_ID) VALUES (:id, :nom, :description, :paysId)";
    private final static String SQL_GET_ALL_VILLE = "SELECT * FROM VILLE";
    private final static String SQL_GET_VILLE_POUR_PAYS = "SELECT * FROM VILLE WHERE PAYS_ID = :paysId;";
    private final static String SQL_GET_VILLE = "SELECT * FROM VILLE WHERE ID = :id;";

    @Inject
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Inject
    private PaysRepository paysRepository;

    public void createVille(Ville ville) {
        var params = new HashMap<String, Object>();

        params.put("id", ville.getId());
        params.put("nom", ville.getNom());
        params.put("description", ville.getDescription());
        params.put("paysId", ville.getPays().getId());

        this.jdbcTemplate.update(SQL_CREATE_VILLE, params);
    }

    public List<Ville> getVilles() {
        List<VilleEntity> list = this.jdbcTemplate.query(SQL_GET_ALL_VILLE, new BeanPropertyRowMapper(VilleEntity.class));

        List<Ville> listVille = new ArrayList<>();
        for(VilleEntity v: list) {
            Ville ville = VilleMapper.toDto(v);

            ville.setPays(paysRepository.getPays(v.getPaysId()));
            listVille.add(ville);
        }

        return listVille;
    }

    public Ville getVille(String id) {
        var params = new HashMap<String, Object>();

        params.put("id", id);

        VilleEntity v = (VilleEntity) this.jdbcTemplate.query(SQL_GET_VILLE, params, new BeanPropertyRowMapper(VilleEntity.class)).get(0);
        Ville ville = VilleMapper.toDto(v);

        ville.setPays(paysRepository.getPays(v.getPaysId()));

        return ville;
    }
}
