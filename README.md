Architectures N-tiers JEE
TOUZÉ Quentin
Site de voyage

GitLab du projet :
https://gitlab.com/TouzeQuentin/voyage

Technologies utilisé :
- Front : Angular
- Back : Spring Boot
- Base de données : MySQL


Fonctions mises en place :
Visiteur (non-connecté) :
- Afficher la liste de voyages
- Afficher les détails d’un voyage

Utilisateur (avec un compte) :
- Se connecter
- Réserver un voyage
- Lire les avis
- Ecrire un avis
- Voir son panier en cours
- Voir l’historique de ses paniers
- Supprimer une réservation du panier
- Valider le panier

Administrateur (avec un compte administrateur) :
- Créer un pays
- Créer un voyage (sans l’image)