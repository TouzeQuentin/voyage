import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterOutlet, RouterLink, RouterLinkActive } from '@angular/router';
import { LoginService } from '../../general/connexion/login.service';

@Component({
  selector: 'app-menu',
  standalone: true,
  imports: [CommonModule, RouterOutlet, RouterLink, RouterLinkActive],
  templateUrl: './menu.component.html',
  styleUrl: './menu.component.scss',
})
export class MenuComponent {
  constructor(private readonly loginService: LoginService) {}

  estConnecte(): boolean {
    // Vérifiez si l'utilisateur est connecté en utilisant le service
    return this.loginService.isLoggedIn();
  }

  estAdmin(): boolean {
    return this.loginService.isAdmin();
  }
}
