import { Component } from '@angular/core';
import { TableauVoyageComponent } from '../../general/voyage/tableau-voyage/tableau-voyage.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-accueil',
  standalone: true,
  imports: [TableauVoyageComponent],
  templateUrl: './accueil.component.html',
  styleUrl: './accueil.component.scss',
})
export class AccueilComponent {
  constructor(private readonly router: Router) {
    this.router.navigateByUrl('voyages');
  }
}
