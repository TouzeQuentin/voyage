import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'date',
  standalone: true,
})
export class DatePipe implements PipeTransform {
  transform(value: Date): string {
    return `${value.getHours}/${value.getDate}/${value.getDate}`;
  }
}
