import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ImageModel } from '../general/models/image';

@Injectable({
  providedIn: 'root',
})
export class ImageService {
  URL_IMAGE = '/voyage/v1/images';
  constructor(private readonly http: HttpClient) {}

  createImage(file: File): Observable<any> {
    const formData = new FormData();

    // Store form name as "file" with file data
    formData.append('file', file, file.name);
    console.log(formData);

    return this.http.post(this.URL_IMAGE, formData);
  }
}
