import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class VilleService {
  URL_VILLE = '/voyage/v1/villes';
  constructor(private readonly http: HttpClient) {}

  getVilles(): Observable<any> {
    return this.http.get(this.URL_VILLE);
  }
}
