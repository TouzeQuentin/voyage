import { Component, OnInit } from '@angular/core';
import { LoginService } from '../login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-deconnexion',
  standalone: true,
  imports: [],
  templateUrl: './deconnexion.component.html',
  styleUrl: './deconnexion.component.scss',
})
export class DeconnexionComponent implements OnInit {
  constructor(
    private readonly loginService: LoginService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.loginService.logout();
    this.router.navigateByUrl('/connexion');
  }
}
