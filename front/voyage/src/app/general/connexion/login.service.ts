import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { UtilisateurModel } from '../models/utilisateur';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  utilisateurConnecte: UtilisateurModel | undefined;

  routeConnexion = '/voyage/v1/utilisateur/connexion';

  constructor(private http: HttpClient) {}

  connexion(utilisateur: UtilisateurModel): Observable<any> {
    return this.http.post<any>(this.routeConnexion, utilisateur);
  }

  isLoggedIn(): boolean {
    if (
      this.utilisateurConnecte === undefined &&
      localStorage.getItem('token')
    ) {
      this.utilisateurConnecte = {
        mail: localStorage.getItem('token') ?? undefined,
      };
    }
    return !!localStorage.getItem('token');
  }

  isAdmin(): boolean {
    return localStorage.getItem('statut') === 'A';
  }

  // Méthode pour connecter l'utilisateur
  login(connexionOk: UtilisateurModel): void {
    if (connexionOk.mail) {
      this.utilisateurConnecte = connexionOk;
      localStorage.setItem('token', connexionOk.mail);
      localStorage.setItem('statut', connexionOk.statut ?? '');
    }
  }

  // Méthode pour déconnecter l'utilisateur
  logout(): void {
    localStorage.removeItem('token');
    this.utilisateurConnecte = undefined;
  }
}
