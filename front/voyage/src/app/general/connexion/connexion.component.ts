import { Component } from '@angular/core';
import { FormBuilder, FormControl, ReactiveFormsModule } from '@angular/forms';
import { LoginService } from './login.service';
import { UtilisateurModel } from '../models/utilisateur';
import { Observable, mergeMap, of, tap } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-connexion',
  standalone: true,
  imports: [ReactiveFormsModule],
  templateUrl: './connexion.component.html',
  styleUrl: './connexion.component.scss',
})
export class ConnexionComponent {
  username = new FormControl('');
  password = new FormControl('');
  messageErreur = '';

  constructor(
    private readonly fb: FormBuilder,
    private readonly loginService: LoginService,
    private readonly router: Router
  ) {}

  connexion() {
    // Placez ici votre logique d'authentification
    console.log("Nom d'utilisateur:", this.username.value);
    console.log('Mot de passe:', this.password.value);

    const utilisateur: UtilisateurModel = {
      mail: this.username.value ?? undefined,
      mdp: this.password.value ?? undefined,
    };
    this.loginService
      .connexion(utilisateur)
      .pipe(
        mergeMap((connexionOk: UtilisateurModel): Observable<boolean> => {
          if (connexionOk && connexionOk.mail && connexionOk.statut) {
            this.loginService.login(connexionOk);
            return of(true);
          }
          return of(false);
        }),
        tap((connexionOk: boolean) => {
          if (connexionOk) {
            this.router.navigateByUrl('/');
          } else {
            this.messageErreur = 'Identifiant ou mot de passe inconue';
          }
        })
      )
      .subscribe();
  }
}
