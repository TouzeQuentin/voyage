import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationVoyageComponent } from './creation-voyage.component';

describe('CreationVoyageComponent', () => {
  let component: CreationVoyageComponent;
  let fixture: ComponentFixture<CreationVoyageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreationVoyageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CreationVoyageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
