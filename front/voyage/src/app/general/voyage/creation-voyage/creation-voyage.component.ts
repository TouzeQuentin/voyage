import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
} from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { VoyageService } from '../voyage-service.service';
import { VoyageModel } from '../../models/voyage';
import { VilleService } from '../../../service/ville.service';
import { Observable, mergeMap, tap } from 'rxjs';
import { VilleModel } from '../../models/ville';
import { NgFor } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { ImageService } from '../../../service/image.service';
import { ImageModel } from '../../models/image';

@Component({
  selector: 'app-creation-voyage',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    ReactiveFormsModule,
    NgFor,
  ],
  templateUrl: './creation-voyage.component.html',
  styleUrl: './creation-voyage.component.scss',
})
export class CreationVoyageComponent implements OnInit {
  villes: VilleModel[] = [];
  get nomControl(): string {
    return this.form.get('nom')?.value;
  }

  get descriptionControl(): string {
    return this.form.get('description')?.value;
  }

  get prixControl(): number {
    return this.form.get('prix')?.value;
  }

  get villeControl(): VilleModel {
    return this.form.get('ville')?.value;
  }

  form: FormGroup;
  constructor(
    private fb: FormBuilder,
    private voyageService: VoyageService,
    private villeService: VilleService,
    private sanitizer: DomSanitizer,
    private readonly imageService: ImageService
  ) {
    this.form = this.fb.group({
      nom: new FormControl(''),
      description: new FormControl(''),
      prix: new FormControl(0),
      ville: new FormControl(undefined),
    });
  }

  ngOnInit(): void {
    this.villeService
      .getVilles()
      .pipe(
        tap((villes: VilleModel[]) => {
          this.villes = villes;
        })
      )
      .subscribe();
  }

  creerVoyage(): void {
    console.log('créer voyage : ', this.nomControl);

    if (this.file) {
      this.imageService
        .createImage(this.file)
        .pipe(
          mergeMap((image: ImageModel): Observable<any> => {
            const model: VoyageModel = {
              id: '',
              nom: this.nomControl,
              description: this.descriptionControl,
              prix: this.prixControl,
              ville: {
                id: this.villeControl.id,
              },
              image: {
                id: image.id,
              },
            };

            return this.voyageService.createVoyage(model);
          }),
          tap(() => {})
        )
        .subscribe();
    } else {
      const model: VoyageModel = {
        id: '',
        nom: this.nomControl,
        description: this.descriptionControl,
        prix: this.prixControl,
        ville: {
          id: this.villeControl.id,
        },
      };
      this.voyageService
        .createVoyage(model)
        .pipe(tap(() => {}))
        .subscribe();
    }
  }

  file: File | undefined; // Variable to store file

  onChange(event: any) {
    this.file = event.target.files[0];
  }
}
