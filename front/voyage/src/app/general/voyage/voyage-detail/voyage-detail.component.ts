import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VoyageService } from '../voyage-service.service';
import { VoyageModel } from '../../models/voyage';
import { EMPTY, catchError, tap } from 'rxjs';
import { log } from 'console';
import { CurrencyPipe, NgIf } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { ReservationComponent } from './reservation/reservation.component';
import { MessageComponent } from './message/message.component';

@Component({
  selector: 'app-voyage-detail',
  standalone: true,
  imports: [NgIf, CurrencyPipe, ReservationComponent, MessageComponent],
  templateUrl: './voyage-detail.component.html',
  styleUrl: './voyage-detail.component.scss',
})
export class VoyageDetailComponent {
  id: string = '';
  voyage: VoyageModel | null = null;

  constructor(
    private route: ActivatedRoute,
    private voyageService: VoyageService,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id') ?? '';
    console.log(this.id);

    this.voyageService
      .getVoyage(this.id)
      .pipe(
        tap((v: VoyageModel) => {
          console.log(v, v.image!.image);

          let objectURL = 'data:image/png;base64,' + v.image!.image;
          v.image!.image = this.sanitizer.bypassSecurityTrustUrl(objectURL);
          this.voyage = v;
        }),
        catchError((e: unknown) => {
          console.log('error', e);
          return EMPTY;
        })
      )
      .subscribe();
  }
}
