import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreationReservationModel } from '../../../models/creation-reservation';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ReservationService {
  URL_RESERVATION = '/voyage/v1/reservation';
  constructor(private readonly http: HttpClient) {}

  creerReservation(reservation: CreationReservationModel): Observable<any> {
    return this.http.post(this.URL_RESERVATION, reservation);
  }

  supprimerReservation(reservation: string): Observable<any> {
    return this.http.delete(`${this.URL_RESERVATION}?id=${reservation}`);
  }
}
