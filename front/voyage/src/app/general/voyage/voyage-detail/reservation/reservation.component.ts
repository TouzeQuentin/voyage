import { Component, Input } from '@angular/core';
import { LoginService } from '../../../connexion/login.service';
import { NgIf } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { FormControl, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ReservationService } from './reservation.service';
import { CreationReservationModel } from '../../../models/creation-reservation';
import { tap } from 'rxjs';
import { ReservationModel } from '../../../models/reservation';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-reservation',
  standalone: true,
  imports: [
    NgIf,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [MatDatepickerModule],
  templateUrl: './reservation.component.html',
  styleUrl: './reservation.component.scss',
})
export class ReservationComponent {
  @Input() voyageId = '';
  dateDebut = new FormControl<Date | undefined>(undefined);
  dateFin = new FormControl<Date | undefined>(undefined);
  dateMin = new Date();

  constructor(
    private readonly loginService: LoginService,
    private readonly reservationService: ReservationService,
    private snackBar: MatSnackBar
  ) {}

  estConnecte(): boolean {
    // Vérifiez si l'utilisateur est connecté en utilisant le service

    return this.loginService.isLoggedIn();
  }

  reserver(): void {
    console.log('debut : ', this.dateDebut.value, ' fin :', this.dateFin.value);

    const resModel: CreationReservationModel = {
      idVoyage: this.voyageId,
      idUtilisateur: this.loginService.utilisateurConnecte?.mail ?? '',
      dateDebut: this.dateDebut.value ?? new Date(),
      dateFin: this.dateFin.value ?? new Date(),
    };
    this.reservationService
      .creerReservation(resModel)
      .pipe(
        tap((reservation: ReservationModel) => {
          console.log('Reservation OK: ', reservation);
          this.snackBar.open(
            'Reservation effectué, elle est consultable dans votre panier',
            'Ok'
          );
        })
      )
      .subscribe();
  }
}
