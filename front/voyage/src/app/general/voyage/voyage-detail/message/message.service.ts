import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { MessageModel } from '../../../models/message';

@Injectable({
  providedIn: 'root',
})
export class MessageService {
  URL_MESSAGE = '/voyage/v1/messages';
  constructor(private readonly http: HttpClient) {}

  getMessages(idVoyage: string): Observable<any> {
    return this.http.get(`${this.URL_MESSAGE}?id=${idVoyage}`);
  }

  createMessages(message: MessageModel): Observable<any> {
    return this.http.post(this.URL_MESSAGE, message);
  }
}
