import { Component, Input, OnInit } from '@angular/core';
import { MessageModel } from '../../../models/message';
import { MessageService } from './message.service';
import { tap } from 'rxjs';
import { NgFor, NgIf } from '@angular/common';
import { MatFormField, MatFormFieldModule } from '@angular/material/form-field';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  FormsModule,
  ReactiveFormsModule,
} from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatInputModule } from '@angular/material/input';
import { LoginService } from '../../../connexion/login.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-message',
  standalone: true,
  imports: [
    NgFor,
    NgIf,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  templateUrl: './message.component.html',
  styleUrl: './message.component.scss',
})
export class MessageComponent implements OnInit {
  listMessage: MessageModel[] = [];

  @Input() voyageId = '';

  get noteControl(): number {
    return this.form.get('note')?.value;
  }

  get textControl(): string {
    return this.form.get('text')?.value;
  }

  form: FormGroup;
  constructor(
    private readonly messageService: MessageService,
    private fb: FormBuilder,
    private readonly loginService: LoginService,
    private readonly snackBar: MatSnackBar
  ) {
    this.form = this.fb.group({
      note: new FormControl(0),
      text: new FormControl(''),
    });
  }

  ngOnInit(): void {
    this.messageService
      .getMessages(this.voyageId)
      .pipe(
        tap((messages: MessageModel[]) => {
          console.log(messages);

          this.listMessage = messages;
        })
      )
      .subscribe();
  }
  estConnecte(): boolean {
    // Vérifiez si l'utilisateur est connecté en utilisant le service

    console.log(this.loginService.isLoggedIn());

    return this.loginService.isLoggedIn();
  }

  publierMessage(): void {
    const m: MessageModel = {
      text: this.textControl,
      note: this.noteControl,
      utilisateurMail: this.loginService.utilisateurConnecte?.mail ?? '',
      voyageId: this.voyageId,
    };

    this.messageService
      .createMessages(m)
      .pipe(
        tap(() => {
          this.snackBar.open('Votre avis a bien été envoyé', 'Ok');
        })
      )
      .subscribe();
  }
}
