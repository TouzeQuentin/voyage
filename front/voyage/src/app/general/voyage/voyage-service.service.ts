import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { VoyageModel } from '../models/voyage';

@Injectable({
  providedIn: 'root',
})
export class VoyageService {
  URL_VOYAGE = '/voyage/v1/voyages';

  constructor(private http: HttpClient) {}

  getVoyages(): Observable<any> {
    return this.http.get<any[]>(this.URL_VOYAGE);
  }

  getVoyage(id: string): Observable<any> {
    return this.http.get<any[]>(`${this.URL_VOYAGE}?id=${id}`);
  }

  createVoyage(model: VoyageModel): Observable<any> {
    return this.http.post(this.URL_VOYAGE, model);
  }
}
