import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TableauVoyageComponent } from './tableau-voyage.component';

describe('TableauVoyageComponent', () => {
  let component: TableauVoyageComponent;
  let fixture: ComponentFixture<TableauVoyageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TableauVoyageComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TableauVoyageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
