import { Component, OnInit } from '@angular/core';
import { VoyageService } from '../voyage-service.service';
import { EMPTY, catchError, tap } from 'rxjs';
import { VoyageModel } from '../../models/voyage';
import { MatCardModule } from '@angular/material/card';
import { NgFor, NgForOf } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
import { RouterOutlet, RouterLink, RouterLinkActive } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-tableau-voyage',
  standalone: true,
  imports: [
    MatCardModule,
    MatButtonModule,
    NgFor,
    NgForOf,
    RouterOutlet,
    RouterLink,
    RouterLinkActive,
  ],
  templateUrl: './tableau-voyage.component.html',
  styleUrl: './tableau-voyage.component.scss',
})
export class TableauVoyageComponent implements OnInit {
  listeVoyage: VoyageModel[] = [];
  constructor(
    private voyageService: VoyageService,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.voyageService
      .getVoyages()
      .pipe(
        tap((listeVoyage: VoyageModel[]) => {
          console.log(listeVoyage);

          for (let voyage of listeVoyage) {
            let objectURL = 'data:image/png;base64,' + voyage.image!.image;
            console.log(voyage.image!.image);

            voyage.image!.image =
              this.sanitizer.bypassSecurityTrustUrl(objectURL);
          }
          this.listeVoyage = listeVoyage;
        }),
        catchError((error) => {
          console.log('error', error);
          return EMPTY;
        })
      )
      .subscribe();
  }
}
