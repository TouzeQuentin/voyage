export interface MessageModel {
  id?: string;
  note?: number;
  text?: string;
  voyageId: string;
  utilisateurMail: string;
}
