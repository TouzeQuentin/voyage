import { PanierModel } from './panier';
import { VoyageModel } from './voyage';

export interface ReservationModel {
  id: string;
  dateDebut: Date;
  dateFin: Date;
  panier: PanierModel;
  voyage: VoyageModel;
}
