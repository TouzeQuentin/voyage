import { ImageModel } from './image';
import { VilleModel } from './ville';

export interface VoyageModel {
  id: string;
  nom?: string;
  description?: string;
  prix?: number;
  ville?: VilleModel;
  image?: ImageModel;
}
