import { ReservationModel } from './reservation';

export interface PanierAvecReservationModel {
  id: string;
  etat: string;
  reservations: ReservationModel[];
}
