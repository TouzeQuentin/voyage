export interface UtilisateurModel {
  mail?: string;
  mdp?: string;
  numero?: string;
  dateCreation?: Date;
  statut?: string;
}
