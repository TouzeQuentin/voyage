import { UtilisateurModel } from './utilisateur';

export interface PanierModel {
  id: string;
  etat: string;
  utilisateur: UtilisateurModel;
}
