import { PaysModel } from './pays';

export interface VilleModel {
  id?: string;
  nom?: string;
  description?: string;
  pays?: PaysModel;
}
