export interface CreationReservationModel {
  idUtilisateur: string;
  idVoyage: string;
  dateDebut: Date;
  dateFin: Date;
}
