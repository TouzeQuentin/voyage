import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginService } from '../connexion/login.service';

@Injectable({
  providedIn: 'root',
})
export class PanierService {
  URL_PANIER = 'voyage/v1/panier';
  constructor(private readonly http: HttpClient) {}

  getPaniersUtilisateur(mail: string): Observable<any> {
    return this.http.get(`${this.URL_PANIER}?mail=${mail}`);
  }

  validerPaniers(id: string): Observable<any> {
    return this.http.put(`${this.URL_PANIER}?id=${id}`, null);
  }
}
