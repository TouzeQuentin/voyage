import { Component, OnInit } from '@angular/core';
import { LoginService } from '../connexion/login.service';
import { DatePipe, NgFor, NgIf } from '@angular/common';
import { PanierService } from './panier.service';
import { tap } from 'rxjs';
import { PanierAvecReservationModel } from '../models/panier-avec-reservation';
import { Router } from '@angular/router';
import {
  MatSnackBar,
  MatSnackBarAction,
  MatSnackBarActions,
  MatSnackBarLabel,
  MatSnackBarRef,
} from '@angular/material/snack-bar';
import { ReservationService } from '../voyage/voyage-detail/reservation/reservation.service';

@Component({
  selector: 'app-panier',
  standalone: true,
  imports: [NgIf, NgFor, DatePipe],
  templateUrl: './panier.component.html',
  styleUrl: './panier.component.scss',
})
export class PanierComponent implements OnInit {
  listePaniers: PanierAvecReservationModel[] = [];
  prixTotalPanier = 0;
  constructor(
    private readonly loginService: LoginService,
    private readonly panierService: PanierService,
    private readonly router: Router,
    private snackBar: MatSnackBar,
    private readonly reservationService: ReservationService
  ) {}

  ngOnInit(): void {
    if (
      this.loginService.isLoggedIn() &&
      this.loginService.utilisateurConnecte?.mail
    ) {
      this.panierService
        .getPaniersUtilisateur(this.loginService.utilisateurConnecte?.mail)
        .pipe(
          tap((paniers: PanierAvecReservationModel[]) => {
            this.listePaniers = paniers;
            paniers
              .filter((panier) => panier.etat === 'C')
              .forEach((panier) => {
                panier.reservations.forEach((res) => {
                  let diffTime =
                    new Date(res.dateFin).getTime() -
                    new Date(res.dateDebut).getTime();
                  let difJours = Math.round(diffTime / (1000 * 3600 * 24));

                  this.prixTotalPanier += (res.voyage.prix ?? 0) * difJours;
                });
              });
            console.log(paniers);
          })
        )
        .subscribe();
    }
  }

  voirVoyage(idVoyage: string): void {
    this.router.navigateByUrl(`/voyage-detail/${idVoyage}`);
  }

  estConnecte(): boolean {
    // Vérifiez si l'utilisateur est connecté en utilisant le service
    return !!this.loginService.isLoggedIn();
  }

  validerPanier(id: string): void {
    this.panierService
      .validerPaniers(id)
      .pipe(
        tap((retour: string) => {
          console.log('ok');

          const url = self ? this.router.url : 'panier';
          this.snackBar.open(
            'Le panier est validé, merci pour votre commande !',
            'Ok'
          );
          this.router
            .navigateByUrl('/', { skipLocationChange: true })
            .then(() => {
              this.router.navigate([`/${url}`]).then(() => {});
            });
        })
      )
      .subscribe();
  }

  supprimerPanier(id: string): void {
    console.log(id);

    this.reservationService
      .supprimerReservation(id)
      .pipe(
        tap(() => {
          this.snackBar.open('La réservation a été annulée.', 'Ok');
        })
      )
      .subscribe();
  }
}
