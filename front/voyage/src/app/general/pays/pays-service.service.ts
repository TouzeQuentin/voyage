import { Injectable } from '@angular/core';
import { PaysModel } from '../models/pays';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class PaysService {
  URL_VOYAGE = '/voyage/v1/pays';

  constructor(private http: HttpClient) {}

  creerPays(pays: PaysModel): Observable<any> {
    return this.http.post<any[]>(this.URL_VOYAGE, pays);
  }
}
