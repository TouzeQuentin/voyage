import { Component } from '@angular/core';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { HttpClient } from '@angular/common/http';
import { MatIconModule } from '@angular/material/icon';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  ReactiveFormsModule,
} from '@angular/forms';
import { PaysModel } from '../../models/pays';
import { PaysService } from '../pays-service.service';

@Component({
  selector: 'app-creation-pays',
  standalone: true,
  imports: [
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatIconModule,
    ReactiveFormsModule,
  ],
  templateUrl: './creation-pays.component.html',
  styleUrl: './creation-pays.component.scss',
})
export class CreationPaysComponent {
  get nomControl(): string {
    return this.form.get('nom')?.value;
  }

  get descriptionControl(): string {
    return this.form.get('description')?.value;
  }

  form: FormGroup;
  constructor(private fb: FormBuilder, private paysService: PaysService) {
    this.form = fb.group({
      nom: new FormControl(''),
      description: new FormControl(''),
    });
  }

  creerPays(): void {
    console.log('créer pays : ', this.nomControl);
    const model: PaysModel = {
      id: '',
      nom: this.nomControl,
      description: this.descriptionControl,
    };

    this.paysService.creerPays(model).subscribe();
  }
}
