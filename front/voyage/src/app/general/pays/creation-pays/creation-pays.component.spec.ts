import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationPaysComponent } from './creation-pays.component';

describe('CreationPaysComponent', () => {
  let component: CreationPaysComponent;
  let fixture: ComponentFixture<CreationPaysComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [CreationPaysComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(CreationPaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
