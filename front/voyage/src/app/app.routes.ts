import { Routes } from '@angular/router';
import { TableauVoyageComponent } from './general/voyage/tableau-voyage/tableau-voyage.component';
import { CreationPaysComponent } from './general/pays/creation-pays/creation-pays.component';
import { AccueilComponent } from './accueil/accueil/accueil.component';
import { VoyageDetailComponent } from './general/voyage/voyage-detail/voyage-detail.component';
import { ConnexionComponent } from './general/connexion/connexion.component';
import { PanierComponent } from './general/panier/panier.component';
import { CreationVoyageComponent } from './general/voyage/creation-voyage/creation-voyage.component';
import { DeconnexionComponent } from './general/connexion/deconnexion/deconnexion.component';

export const routes: Routes = [
  {
    path: 'accueil',
    component: AccueilComponent,
  },
  {
    path: 'voyages',
    title: 'Voyage',
    component: TableauVoyageComponent,
  },
  {
    path: 'voyage-detail/:id', // child route path
    component: VoyageDetailComponent, // child route component that the router renders
  },
  {
    path: 'pays',
    component: CreationPaysComponent,
  },
  {
    path: 'creation-voyage',
    component: CreationVoyageComponent,
  },
  {
    path: 'panier',
    component: PanierComponent,
  },
  {
    path: 'connexion',
    component: ConnexionComponent,
  },
  {
    path: 'deconnexion',
    component: DeconnexionComponent,
  },
  { path: '', redirectTo: '/accueil', pathMatch: 'full' },
];
